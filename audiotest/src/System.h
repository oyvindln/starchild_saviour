/*
 * System.h
 *
 *  Created on: 28. sep. 2010
 *      Author: oln
 */

#ifndef SYSTEM_H
#define SYSTEM_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Enemy.h"
#include "Game_sound_recorder.h"
#include "Player.h"
#include "util.h"
#include <iostream>
#include <memory>
#include <string>

class System {
public:
  System();

  void move_enemies(float delta);
  bool is_colliding(const sf::Vector2f &a, float radiusA, const sf::Vector2f &b,
                    float radiusB);
  void next_status();

private:
  enum Game_status { SPLASH, HELP, PITCHCOUNT, CALIBRATE, RUNNING, LOST };

  void restart();
  void lose_life();

  sf::RenderWindow app;

  sf::Sprite sun;
  sf::Texture sun_image;
  Player player;
  sf::Sprite background;
  sf::Texture background_image;
  sf::Texture intro_screen_image1;
  sf::Texture intro_screen_image2;
  sf::Texture intro_screen_image3;
  sf::Texture intro_screen_image4;
  sf::Texture help_text_image;
  sf::Sprite intro_screen1;
  sf::Sprite intro_screen2;
  sf::Sprite intro_screen3;
  sf::Sprite intro_screen4;
  sf::Sprite pitch_meter;
  sf::Texture pitch_meter_image;
  sf::Texture pitch_meter_block_image;
  sf::Sprite pitch_meter_block;
  sf::Texture give_count_down_image;
  sf::Texture count_down_finishedimage;
  sf::Sprite give_count_down;

  Image_vector count_down_images;
  sf::Sprite count_down_sprite;

  float sun_radius;
  float player_radius;

  float pitch;
  float delta;

  std::vector<std::unique_ptr<Enemy_type>> enemy_types;
  std::vector<Enemy> attackers;

  SpriteVector life_sprites;

  Game_status status;

  unsigned lives_left;

  sf::Vector2f orig_pos;

  Game_sound_recorder rec;

  sf::Clock hit_count_down;
  sf::Clock intro_timer;
  int count_down_time;

  bool player_is_hit;

  sf::Music music;
};

#endif // SYSTEM_H
