/*
 * Timer_helper.cpp
 *
 *  Created on: 30. jan. 2011
 *      Author: oln
 */

#include "Timer_helper.h"

Timer_helper::Timer_helper() : added_time(0) {}

float Timer_helper::get_elapsed_time() {
  return clock.getElapsedTime().asSeconds() + added_time;
}

void Timer_helper::add_time(float add) { added_time += add; }

void Timer_helper::reset() {
  clock.restart();
  added_time = 0;
}
