

set(SOURCE_FILES
audiotest.cpp
Enemy.cpp
Enemy_type.cpp
Game_sound_recorder.cpp
Player.cpp
System.cpp
Timer_helper.cpp
util.cpp
)

#adding WIN32 prevents console window on windows
add_executable(${PROJECT_BINARY_NAME} WIN32 ${SOURCE_FILES})

target_compile_features(${PROJECT_BINARY_NAME} PRIVATE cxx_range_for cxx_generic_lambdas)

#TODO: Locate aubio properly
target_link_libraries(${PROJECT_BINARY_NAME}
${SFML_LIBRARIES}
aubio
)
