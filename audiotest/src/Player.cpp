/*
 * Player.cpp
 *
 *  Created on: 30. jan. 2011
 *      Author: oln
 */

#include "Player.h"
#include <iostream>

Player::Player() {}

void Player::load(const String_list &imageFilenames,
                  const std::string &hit_sprite_name, unsigned fps,
                  float radius) {
  current_frame = 0;
  frame_time = 1.0f / static_cast<float>(fps);

  for (auto &name : imageFilenames) {
    images.push_back(sf::Texture());
    if (images.back().loadFromFile(name)) {
      std::cout << "Loaded: " << name << std::endl;
    }
  }

  sf::Vector2f sprite_scaling = calculate_scaling(images[0], radius);
  for (auto &image : images) {
    sf::Sprite s(image);
    s.setOrigin(get_center(s));
    s.setScale(sprite_scaling);
    sprites.emplace_back(s);
  }

  hit_sprite_image.loadFromFile(hit_sprite_name);
  hit_sprite.setTexture(hit_sprite_image);
  hit_sprite.setScale(calculate_scaling(hit_sprite_image, radius + 10));
  hit_sprite.setOrigin(get_center(hit_sprite));
}

const sf::Sprite &Player::get_sprite() { return sprites[current_frame]; }

const sf::Sprite &Player::get_hit_sprite() {
  hit_sprite.setPosition(sprites[current_frame].getPosition());
  return hit_sprite;
}

void Player::update_anim() {
  float elTime = anim_timer.get_elapsed_time();
  if (elTime > frame_time) {
    unsigned oldFrame = current_frame;
    ++current_frame;
    if (current_frame >= sprites.size()) {
      current_frame = 0;
    }
    sprites[current_frame].setPosition(sprites[oldFrame].getPosition());
    anim_timer.reset();
    anim_timer.add_time(elTime - frame_time);
  }
}

void Player::set_position(float x, float y) {
  sprites[current_frame].setPosition(x, y);
}

void Player::set_rotation(float rotation) {
  sprites[current_frame].setRotation(rotation);
}
