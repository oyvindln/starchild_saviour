/*
 * Game_sound_recorder.cpp
 *
 *  Created on: 28. jan. 2011
 *      Author: oln
 */
#include "Game_sound_recorder.h"
#include <cmath>
#include <cstring>
#include <iostream>

#include <gsl.h>

namespace {
const constexpr size_t buffer_size = 4096;
const constexpr uint_t win_s = buffer_size * 4;
const constexpr uint_t hop_s = buffer_size;
const constexpr uint_t sample_rate = 44100;
// The method name has to be mutable as older versions of aubio have a non-const
// char_t parameter
// It is not actually modified in the function
char default_name[] = "default";
}

Game_sound_recorder::Game_sound_recorder()
    : pitch_detector(new_aubio_pitch(default_name, win_s, hop_s, sample_rate),
                     del_aubio_pitch),
      buffer(new_fvec(hop_s), del_fvec),
      detector_output(new_fvec(1), del_fvec) {
  threshold = 400;
}

float Game_sound_recorder::get_pitch() {
  sf::Lock lock(read_lock);
  return pitch;
}

float Game_sound_recorder::get_calibration_pitch() { return calibration_pitch; }

void Game_sound_recorder::set_calibration_pitch(float sc_pitch) {
  this->calibration_pitch = sc_pitch;
}

bool Game_sound_recorder::onStart() { return true; }

void Game_sound_recorder::onStop() {
  //
  std::cout << "Stopping" << std::endl;
}

bool Game_sound_recorder::onProcessSamples(const sf::Int16 *Samples,
                                           size_t SamplesCount) {
  gsl::span<const sf::Int16> samples{Samples,
                                     static_cast<int64_t>(SamplesCount)};
  gsl::span<smpl_t> buffer_span{buffer->data, buffer->length};

  if (samples.length() >= buffer_span.length()) {

    sf::Int16 max = 0;
    for (auto i = 0; i < buffer_span.length(); ++i) {
      if (samples[i] > max) {
        max = samples[i];
      }
      //            buffer->data[0][i] = Samples[i];
      buffer_span[i] = samples[i];
      //            buffer[i * 2] = Samples[i];
      //            buffer[(i * 2) + 1] = 1;
    }

    {
      sf::Lock lock2(peak_lock);
      lastPeak = max;
    }

    sf::Lock lock(read_lock);
    if (max >= threshold) {
      aubio_pitch_do(pitch_detector.get(), buffer.get(), detector_output.get());
      pitch = detector_output->data[0];
      //            fftss_execute(plan);
      //            pitch = aubio_pitchdetection(pitchDetector, buffer);
      std::cout << "Pitch: " << pitch << " SamplesCount " << SamplesCount
                << std::endl;
      std::cout << "Peak: " << max << std::endl;
      //            pitch = findPitch();
      //            std::cout << "Pitch: "<< pitch << " SamplesCount " <<
      //            SamplesCount << std::endl;
    } else {
      // std::cout << "Below threshold" << std::endl;
    }

  } else {
    std::cout << "SamplesCount " << SamplesCount << std::endl;
  }
  return true;
}

sf::Int16 Game_sound_recorder::get_threshold() const { return threshold; }

void Game_sound_recorder::set_threshold(sf::Int16 threshold) {
  this->threshold = threshold;
}

float Game_sound_recorder::get_last_peak() {
  sf::Lock lock(peak_lock);
  return lastPeak;
}

void Game_sound_recorder::clear_peak() {
  sf::Lock lock(peak_lock);
  lastPeak = 0;
}
/*
void Game_sound_recorder::update()
{

}

float Game_sound_recorder::findPitch()
{
    const unsigned cutoff = 120;
    float out = 0;
    float max = -INFINITY;
    for(int i = 0; i < cutoff; ++i)
    {
        if (buffer[i * 2] > max)
        {
            max = buffer[i * 2];
            out = i;
        }
    }
    return out;
}
*/
