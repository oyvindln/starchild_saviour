/*
 * Enemy.h
 *
 *  Created on: 29. jan. 2011
 *      Author: oln
 */

#ifndef ENEMY_H
#define ENEMY_H

#include "Enemy_type.h"
#include "Timer_helper.h"
#include <SFML/Graphics.hpp>
#include <list>

using SpriteVector = std::vector<sf::Sprite>;

class Enemy {
public:
  explicit Enemy(const Enemy_type &enemy_type);
  Enemy(Enemy &&) noexcept = default;
  Enemy &operator=(Enemy &&) noexcept = default;

  sf::Sprite &get_sprite();
  sf::Sprite &get_tail_sprite();
  const Enemy_type &get_type() const;
  const sf::Vector2f &get_velocity() const;

  // Actually velocity
  void set_velocity(const sf::Vector2f &velocity);
  void move(float delta);
  // sf::Vector2f getCenter();
  void set_position(const sf::Vector2f &position);

private:
  /*    void addToClock(sf::Clock& clock, float time)
	  {
		  clock. += time;
	  }*/
  Enemy(const Enemy &) = delete;
  Enemy &operator=(const Enemy &) = delete;

  const Enemy_type *type;
  SpriteVector sprites;
  SpriteVector tail_sprites;

  unsigned fps;
  float frame_time;
  unsigned current_frame;
  Timer_helper anim_timer;

  sf::Vector2f velocity;
  sf::Vector2f offset;
};

#endif // ENEMY_H
