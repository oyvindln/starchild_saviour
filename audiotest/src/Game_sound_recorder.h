/*
 * Game_sound_recorder.h
 *
 *  Created on: 28. jan. 2011
 *      Author: oln
 */

#ifndef GAMESOUNDRECORDER_H
#define GAMESOUNDRECORDER_H

#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include <aubio/aubio.h>
#include <aubio/types.h>
#include <vector>

#include <climits>
#include <memory>

namespace {
using pitch_ptr = std::unique_ptr<aubio_pitch_t, decltype(&del_aubio_pitch)>;
using fvec_ptr = std::unique_ptr<fvec_t, decltype(&del_fvec)>;
}

class Game_sound_recorder : public sf::SoundRecorder {
public:
  Game_sound_recorder();
  float get_pitch();
  sf::Int16 get_threshold() const;
  void set_threshold(sf::Int16 threshold);

  float get_calibration_pitch();
  void set_calibration_pitch(float sc_pitch);

  float get_last_peak();
  void clear_peak();

  void update();

protected:
  bool onStart() override;
  void onStop() override;
  bool onProcessSamples(const sf::Int16 *Samples,
                        std::size_t SamplesCount) override;

private:
  pitch_ptr pitch_detector;
  //    aubio_pitch_t* pitchDetector;
  fvec_ptr buffer;
  fvec_ptr detector_output;
  smpl_t pitch;

  float calibration_pitch;
  float lastPeak;

  sf::Mutex read_lock;
  sf::Mutex peak_lock;
  sf::Int16 threshold;
};

#endif // GAMESOUNDRECORDER_H
