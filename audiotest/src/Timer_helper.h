/*
 * Timer_helper.h
 *
 *  Created on: 30. jan. 2011
 *      Author: oln
 */

#ifndef TIMERHELPER_H
#define TIMERHELPER_H

#include <SFML/System.hpp>

class Timer_helper {
public:
  Timer_helper();
  void add_time(float add);
  void reset();
  float get_elapsed_time();

private:
  sf::Clock clock;
  float added_time;
};

#endif // TIMERHELPER_H
