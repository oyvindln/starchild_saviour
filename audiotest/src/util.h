/*
 * util.h
 *
 *  Created on: 29. jan. 2011
 *      Author: oln
 */

#ifndef UTIL_H
#define UTIL_H
#include <SFML/Graphics.hpp>
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif
const float RADTODEG = 180 / M_PI;

sf::Vector2f calculate_scaling(const sf::Texture &image, float radius);
sf::Vector2f get_center(const sf::Sprite &sprite);

#endif // UTIL_H
