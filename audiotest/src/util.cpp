/*
 * util.cpp
 *
 *  Created on: 29. jan. 2011
 *      Author: oln
 */

#include "util.h"

sf::Vector2f calculate_scaling(const sf::Texture &image, float radius) {
  sf::Vector2f scaling;
  scaling.x = (radius * 2) / image.getSize().x;
  scaling.y = scaling.x;
  return scaling;
}

sf::Vector2f get_center(const sf::Sprite &sprite) {
  auto int_ret = sprite.getTexture()->getSize() / 2u;
  // ret.y = sprite.GetImage()->GetHeight() / 2.0f
  return sf::Vector2f(int_ret.x, int_ret.y);
}
