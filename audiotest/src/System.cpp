/*
 * System.cpp
 *

 *      Author: oln
 */
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <random>

#include "System.h"

namespace {

static const sf::Time one_second = sf::seconds(1.0f);

enum Edge { top = 0, right = 1, bottom = 2, left = 3 };

using uint_random = std::uniform_int_distribution<unsigned>;
using float_random = std::uniform_real_distribution<float>;

Enemy spawn_enemy(const Enemy_type &type,
                  std::default_random_engine &random_generator,
                  sf::Vector2u view_size) {
  auto edge = uint_random(0, 3)(random_generator);
  float x, y = 0;
  float velX, velY = 0;
  float speed = type.get_speed();
  auto random_speed = float_random(-speed, speed)(random_generator);
  switch (edge) {
  case (Edge::top):
    y = 0;
    x = uint_random(0, view_size.x)(random_generator);

    velX = random_speed;
    velY = speed;
    break;
  case (Edge::bottom):
    y = view_size.y - type.get_radius();
    x = uint_random(0, view_size.x)(random_generator);

    velX = random_speed;
    velY = -speed;
    break;
  case (Edge::left):
    y = uint_random(0, view_size.y)(random_generator);
    x = 0;

    velX = 10;
    velY = random_speed;
    break;
  case (Edge::right):
    y = uint_random(0, view_size.y)(random_generator);
    x = view_size.x - type.get_radius();

    velX = -speed;
    velY = random_speed;
    break;
  default:
    // Should not get here
    std::terminate();
  }

  Enemy e(type);

  e.set_position(sf::Vector2f(x, y));
  e.set_velocity(sf::Vector2f(velX, velY));
  return e;
}

} // namespace
System::System()
    : app(sf::VideoMode(1024, 768, 32), "Starchild saviour"),
      sun_radius(87.5f) {
  status = SPLASH;

  lives_left = 5;

  // Music is disabled for now, until we have some proper music
  //    music.openFromFile("music.ogg");

  auto hit_image_time = sf::seconds(0.2f);

  app.setFramerateLimit(10);

  if (!rec.isAvailable()) {
    std::cout << "Cannot capture!" << std::endl;
    std::terminate();
  }

  count_down_time = 5;

  const sf::FloatRect &bound = app.getView().getViewport(); // getrect

  String_list comet_names;
  comet_names.push_back("Komet1.png");
  comet_names.push_back("Komet2.png");
  comet_names.push_back("Komet3.png");
  String_list comet_tail_names;
  comet_tail_names.push_back("KometHale1.png");
  comet_tail_names.push_back("KometHale2.png");
  comet_tail_names.push_back("KometHale3.png");
  enemy_types.emplace_back(std::make_unique<Enemy_type>(
      "comet", comet_names, comet_tail_names, 25, 100, 100));

  // enemyTypes[0]->setTailImage("Komet1.png");

  // Create a random generator with a seed based on current time
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine random_generator(seed);

  player_radius = 25;

  String_list player_images;
  player_images.push_back("Planeter_closeUp1.png");
  player_images.push_back("Planeter_closeUp2.png");
  player_images.push_back("Planeter_closeUp3.png");
  player_images.push_back("Planeter_closeUp4.png");
  player_images.push_back("Planeter_closeUp5.png");
  player_images.push_back("Planeter_closeUp6.png");
  player_images.push_back("Planeter_closeUp7.png");
  player_images.push_back("Planeter_closeUp8.png");
  player_images.push_back("Planeter_closeUp9.png");
  player.load(player_images, "Planeter_hit1.png", 3, player_radius);

  sun_image.loadFromFile("Sol_01.png");
  sun.setTexture(sun_image);
  sun.setPosition(app.getView().getSize() / 2.0f);
  sun.setScale(sf::Vector2f(0.1, 0.1));

  background_image.loadFromFile("Papir2.png");
  background.setTexture(background_image);
  background.setScale(0.5, 0.5);

  pitch_meter_image.loadFromFile("PitchMeasure_Meter2.png");
  pitch_meter.setTexture(pitch_meter_image);
  pitch_meter.setPosition(bound.width - pitch_meter.getTexture()->getSize().x,
                          0);
  pitch_meter.setScale(bound.height / pitch_meter.getTexture()->getSize().y,
                       bound.height / pitch_meter.getTexture()->getSize().y);

  pitch_meter_block_image.loadFromFile("PitchMeasure_Arrow_flip.png");
  pitch_meter_block.setTexture(pitch_meter_block_image);
  pitch_meter_block.setPosition(bound.width - 200,
                                app.getView().getSize().y / 2);

  sf::Vector2f player_scaling =
      calculate_scaling(*player.get_sprite().getTexture(), player_radius);
  for (unsigned i = 0; i < lives_left; ++i) {
    sf::Sprite ls(*player.get_sprite().getTexture());
    ls.setScale(player_scaling);
    ls.setPosition(
        20.0f + (i * ((ls.getTexture()->getSize().x * player_scaling.x) + 10)),
        20.0f);
    life_sprites.emplace_back(ls);
  }

  orig_pos.x = 0.0f;
  orig_pos.y = 100.0f;
  auto spawn_time = sf::seconds(1.0f);

  sf::Font font;
  font.loadFromFile("Ubuntu-R.ttf");

  sf::Text lost_text("You lost", font);
  sf::Text calibration_text("calibrating\ngame will start in\n", font);
  sf::Text calibration_complete_text("calibration done", font);

  sf::Clock clock;
  sf::Clock program_time;
  sf::Clock spawn_clock;
  program_time.restart();
  clock.restart();

  sf::CircleShape debug_test(enemy_types[0]->get_radius());

  debug_test.setFillColor(sf::Color(0, 255, 255, 150));

  sun.setOrigin(sun.getTexture()->getSize().x / 2.0f,
                sun.getTexture()->getSize().y / 2.0f);

  lost_text.setScale(3, 3);
  lost_text.setPosition(bound.width / 3, bound.height / 3);
  calibration_text.setScale(3, 3);
  calibration_text.setPosition(bound.width / 3, bound.height / 3);
  calibration_complete_text.setScale(3, 3);
  calibration_complete_text.setPosition(bound.width / 3, bound.height / 3);

  float reference = 0;

  app.clear();

  intro_screen_image1.loadFromFile("Title_screen_BackDrop.png");
  intro_screen_image2.loadFromFile("Title_screen_Characters.png");
  intro_screen_image3.loadFromFile("Title_screen_TitleText2.png");
  intro_screen1.setTexture(intro_screen_image1);
  intro_screen2.setTexture(intro_screen_image2);
  intro_screen3.setTexture(intro_screen_image3);
  help_text_image.loadFromFile("Instructions_Text.png");

  give_count_down_image.loadFromFile("Keynote_screen_text1.png");
  count_down_finishedimage.loadFromFile("Keynote_screen_text1.png");

  for (int i = 0; i < 6; ++i) {
    count_down_images.push_back(sf::Texture());
  }
  count_down_images[0].loadFromFile("Keynote_screen_keynote1.png");
  count_down_images[1].loadFromFile("Keynote_screen_tall1.png");
  count_down_images[2].loadFromFile("Keynote_screen_tall2.png");
  count_down_images[3].loadFromFile("Keynote_screen_tall3.png");
  count_down_images[4].loadFromFile("Keynote_screen_tall4.png");
  count_down_images[5].loadFromFile("Keynote_screen_tall5.png");

  //	music.play();

  intro_timer.restart();
  sf::Clock frame_timer;
  while (app.isOpen()) {
    app.clear();

    auto delta = frame_timer.restart().asSeconds();

    sf::Event event;
    while (app.pollEvent(event)) {
      switch (event.type) {
      case (sf::Event::Closed): {
        app.close();
        break;
      }
      case (sf::Event::KeyPressed): {
        if (event.key.code == sf::Keyboard::Space) {
        }
      }
      default: { break; }
      }
    }

    if (status == RUNNING) {

      if (spawn_clock.getElapsedTime() > spawn_time) {
        spawn_clock.restart();
        Enemy e = spawn_enemy(*enemy_types[0], random_generator, app.getSize());
        attackers.emplace_back(std::move(e));
      }

      pitch = rec.get_pitch();
      float move_planet = 0;
      if (pitch > reference && pitch < 1000) {
        move_planet = 200 * delta;
      }

      else if (pitch < reference) // && pitch > 5)
      {
        move_planet = -200 * delta;
      }

      pitch_meter_block.setPosition(pitch_meter_block.getPosition().x,
                                    app.getView().getSize().y / 2 +
                                        ((reference - pitch) * 5));

      orig_pos.x += move_planet;
      orig_pos.y += move_planet;
      if (orig_pos.x < 50) {
        orig_pos.x = 50;
      }
      if (orig_pos.y < 50) {
        orig_pos.y = 50;
      }
      if (orig_pos.x > app.getSize().x / 4) {
        orig_pos.x = app.getSize().x / 4;
      }
      // TODO(oln): Should this be y rather?
      if (orig_pos.y > app.getSize().x / 4) {
        orig_pos.y = app.getSize().x / 4;
      }

      sf::Vector2f newPos;

      float theta = -program_time.getElapsedTime().asSeconds();
      newPos.x = (orig_pos.x * std::sin(theta)) +
                 (orig_pos.y * std::cos(theta)) + sun.getPosition().x;
      newPos.y = (orig_pos.x * std::cos(theta)) -
                 (orig_pos.y * std::sin(theta)) + sun.getPosition().y;
      player.set_rotation((theta * RADTODEG) - 30); // 180 / pi
      player.set_position(newPos.x, newPos.y);

      sun.setRotation(theta * RADTODEG);

      app.draw(background);
      app.draw(pitch_meter);
      app.draw(pitch_meter_block);
      for (unsigned i = 0; i < lives_left; ++i) {

        app.draw(life_sprites[i]);
      }
      app.draw(sun);

      move_enemies(delta);

      for (auto &enemy : attackers) {
        app.draw(enemy.get_tail_sprite());
        app.draw(enemy.get_sprite());
      }

      player.update_anim();
      if (player_is_hit) {
        app.draw(player.get_hit_sprite());
        if (hit_count_down.getElapsedTime() > hit_image_time) {
          player_is_hit = false;
        }
      } else {
        app.draw(player.get_sprite());
      }
    } else if (status < RUNNING) {

      app.draw(intro_screen1);
      app.draw(intro_screen3);
      if (status == SPLASH) {

        if (intro_timer.getElapsedTime() > one_second) {
          intro_screen3.setTexture(help_text_image);
          next_status();
          intro_timer.restart();
        }
      } else if (status == HELP) {

        if (intro_timer.getElapsedTime() > one_second) {
          //					helpTextImage.loadFromMemory(0,
          // 0);
          next_status();
          intro_timer.restart();
          intro_screen3.setTexture(give_count_down_image);
        }
      } else if (status == PITCHCOUNT) {

        app.draw(count_down_sprite);
        if (intro_timer.getElapsedTime() > one_second) {
          intro_timer.restart();
          if (count_down_time < 0) {
            rec.start(44100);
            next_status();
            intro_screen3.setTexture(count_down_finishedimage);
            //						giveCountDownImage.loadFromMemory(0,
            // 0);
          } else {
            count_down_sprite.setTexture(
                count_down_images[static_cast<size_t>(count_down_time)]);
            --count_down_time;
          }
        }
      } else if (status == CALIBRATE) {
        if (intro_timer.getElapsedTime().asSeconds() > 0.1) {
          if (reference <= 0.00000f) {
            float tempsmpl = rec.get_pitch();
            rec.set_calibration_pitch(tempsmpl);
            reference = rec.get_calibration_pitch();
            std::cout << "calibration done, hopefully\n";
            std::cout << "reference is: " << reference << std::endl;
          }
        }

        if (intro_timer.getElapsedTime() > one_second) {
          intro_screen3.setTexture(sun_image);
          count_down_images.clear();
          next_status();
          app.setFramerateLimit(60);
        }
      }
    } else {
      app.draw(lost_text);
      if (rec.get_last_peak() > 20000) {
        restart();
      }
    }

    app.display();
  }
  rec.stop();
}

void System::move_enemies(float delta) {
  auto it = attackers.begin();
  sf::Vector2f player_center = player.get_sprite().getPosition();
  while (it != attackers.end()) {
    Enemy &temp = *it;
    temp.move(delta);
    const sf::Vector2f &position = temp.get_sprite().getPosition();
    if (position.x > app.getSize().x || position.y > app.getSize().y) {
      it = attackers.erase(it);
      std::cout << "Attacker ran away" << std::endl;
    } else {
      // TODO(oln): using getSize here is incorrect
      if (is_colliding(temp.get_sprite().getPosition(),
                       temp.get_sprite().getTexture()->getSize().x / 2,
                       player_center, player_radius)) {
        it = attackers.erase(it);
        lose_life();
        rec.clear_peak();
      } else {
        ++it;
      }
    }
  }
}

bool System::is_colliding(const sf::Vector2f &a, float radiusA,
                          const sf::Vector2f &b, float radiusB) {
  float dist =
      std::sqrt(((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y)));
  return dist <= radiusA + radiusB;
}

void System::restart() {
  status = RUNNING;
  attackers.clear();
  orig_pos.x = 50;
  orig_pos.y = 50;
  lives_left = 5;
}

void System::lose_life() {
  --lives_left;
  if (lives_left <= 0) {
    status = LOST;
  }
  player_is_hit = true;
  hit_count_down.restart();
}

void System::next_status() {
  status = static_cast<Game_status>(static_cast<int>(status) + 1);
}
