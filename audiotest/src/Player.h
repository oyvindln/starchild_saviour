/*
 * Player.h
 *
 *  Created on: 30. jan. 2011
 *      Author: oln
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "Enemy.h"
#include "util.h"
#include <SFML/Graphics.hpp>

class Player {
public:
  Player();
  void load(const String_list &image_filenames,
            const std::string &hit_sprite_name, unsigned fps, float radius);
  const sf::Sprite &get_sprite();
  const sf::Sprite &get_hit_sprite();
  void update_anim();
  void set_rotation(float rotation);
  void set_position(float x, float y);

private:
  SpriteVector sprites;
  Image_vector images;
  sf::Sprite hit_sprite;
  sf::Texture hit_sprite_image;

  float frame_time;
  unsigned current_frame;
  Timer_helper anim_timer;
};

#endif // PLAYER_H
