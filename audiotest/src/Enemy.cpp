/*
 * Enemy.cpp
 *
 *  Created on: 29. jan. 2011
 *      Author: oln
 */

#include "Enemy.h"
#include "util.h"
#include <cmath>
#include <iostream>

Enemy::Enemy(const Enemy_type &enemy_type)
	: type(&enemy_type), fps(9), frame_time(1.0 / static_cast<float>(fps)),
	  current_frame(0) {
  sf::Vector2f sprite_scaling =
	  calculate_scaling(enemy_type.get_image()[0], enemy_type.get_radius());
  sf::Vector2f tail_sprite_scaling = calculate_scaling(
	  enemy_type.get_tail_image()[0], enemy_type.get_radius());

  for (size_t i = 0; i < enemy_type.get_image().size(); ++i) {
    {
	  sf::Sprite sprite(enemy_type.get_image()[i]);
	  sprite.setOrigin(get_center(sprite));
	  sprite.setScale(sprite_scaling);
	  sprites.emplace_back(sprite);
	}
	{
	  sf::Sprite tail_sprite(enemy_type.get_tail_image()[i]);
	  tail_sprite.setOrigin(get_center(tail_sprite));
	  tail_sprite.setScale(tail_sprite_scaling);
	  tail_sprites.emplace_back(tail_sprite);
	}
  }
}

sf::Sprite &Enemy::get_sprite() { return sprites[current_frame]; }

const Enemy_type &Enemy::get_type() const { return *type; }

const sf::Vector2f &Enemy::get_velocity() const { return velocity; }

void Enemy::set_velocity(const sf::Vector2f &velocity) {
  sf::Sprite &sprite = sprites[0];
  this->velocity = velocity;
  float length =
	  std::sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y));
  offset.x = -(velocity.x / length) * 100 * sprite.getScale().x;
  offset.y = -(velocity.y / length) * 100 * sprite.getScale().y;
  offset.x -= (-velocity.y / length) * 60 * sprite.getScale().y;
  offset.y -= (velocity.x / length) * 60 * sprite.getScale().x;

  for (size_t i = 0; i < tail_sprites.size(); ++i) {
	sprites[i].setRotation(std::atan2(velocity.x, velocity.y) * RADTODEG);
	tail_sprites[i].setRotation(std::atan2(velocity.x, velocity.y) * RADTODEG);
  }
}

void Enemy::move(float delta) {
  float elTime = anim_timer.get_elapsed_time();
  if (elTime > frame_time) {
	unsigned old_frame = current_frame;
	++current_frame;
	if (current_frame >= sprites.size()) {
	  current_frame = 0;
    }
	sprites[current_frame].setPosition(sprites[old_frame].getPosition());
	anim_timer.reset();
	anim_timer.add_time(elTime - frame_time);
  }
  sprites[current_frame].move(velocity * delta);
  tail_sprites[current_frame].setPosition(sprites[current_frame].getPosition() +
										  offset);
  // tailSprite.Rotate(sf::Randomizer::Random(-5, 5));
}

/*sf::Vector2f Enemy::getCenter()
{
    sf::Vector2f ret;
    ret.x = sprite.GetImage()->GetHeight() / 2.0f;
    ret.y = sprite.GetImage()->GetHeight() / 2.0f;
    return ret;
}*/

void Enemy::set_position(const sf::Vector2f &position) {
  sprites[current_frame].setPosition(position);
  tail_sprites[current_frame].setPosition(position + offset);
}

sf::Sprite &Enemy::get_tail_sprite() { return tail_sprites[current_frame]; }
