/*
 * Enemy_type.cpp
 *
 *  Created on: 29. jan. 2011
 *      Author: oln
 */

#include "Enemy_type.h"
#include <cassert>
#include <iostream>

Enemy_type::Enemy_type(std::string name, const String_list &image_filenames,
					   const String_list &second_image_filenames,
					   float radius = 20.0f, float speed = 10.0f,
					   float damage = 10.0f)
	: name(std::move(name)), radius(radius), speed(speed), damage(damage) {
  assert(image_filenames.size() == second_image_filenames.size());

  for (auto &filename : image_filenames) {
	images.push_back(sf::Texture());
	if (images.back().loadFromFile(filename)) {
	  std::cout << "Loaded: " << filename << std::endl;
    }
  }
  for (auto &filename : second_image_filenames) {
	second_images.push_back(sf::Texture());
	if (second_images.back().loadFromFile(filename)) {
	  std::cout << "Loaded: " << filename << std::endl;
    }
  }
}

float Enemy_type::get_damage() const { return damage; }

const Image_vector &Enemy_type::get_image() const { return images; }

std::string Enemy_type::get_name() const { return name; }

float Enemy_type::get_radius() const { return radius; }

float Enemy_type::get_speed() const { return speed; }

void Enemy_type::set_damage(float damage) { this->damage = damage; }

void Enemy_type::set_name(const std::string &name) { this->name = name; }

void Enemy_type::set_radius(float radius) { this->radius = radius; }

void Enemy_type::set_speed(float speed) { this->speed = speed; }

const Image_vector &Enemy_type::get_tail_image() const { return second_images; }
