/*
 * Enemy_type.h
 *
 *  Created on: 29. jan. 2011
 *      Author: oln
 */

#ifndef ENEMYTYPE_H
#define ENEMYTYPE_H

#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>
typedef std::vector<std::string> String_list;
typedef std::vector<sf::Texture> Image_vector;

class Enemy_type {
public:
  explicit Enemy_type(std::string name, const String_list &image_filenames,
                      const String_list &second_image_filenames, float radius,
                      float speed, float damage);
  float get_damage() const;
  const Image_vector &get_image() const;
  std::string get_name() const;
  float get_radius() const;
  float get_speed() const;
  void set_damage(float damage);
  void set_name(const std::string &name);
  void set_radius(float radius);
  void set_speed(float speed);
  const Image_vector &get_tail_image() const;

private:
  Enemy_type(const Enemy_type &) = delete;
  Enemy_type &operator=(const Enemy_type &) = delete;

  std::string name;
  Image_vector images;
  Image_vector second_images;

  float radius;
  float speed;
  float damage;
};

#endif // ENEMYTYPE_H
