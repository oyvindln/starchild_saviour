A game using sound pitch for controls, originally created for Global game jam 2011. 

The source code is licensed under the GPLv3 (LICENSE

Media files (.png) are under CC BY-NC-SA 3.0

A binary version and other information about the submission can be found at http://archive.globalgamejam.org/2011/starchild-saviors